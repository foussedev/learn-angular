# Les bases d'angular
## Les composants
Uncomposant est une classe typescript décoré par @Component de angular
-@Component prend en parametre un objet contenant trois clé
 `templateUrl `: Le path de la page html du composant
 `selector`:Un selecteur css que sera utilisé pour afficher le composant Ex:app-naber
 `styleUrls`:Un tableau contenant les paths des fichiers css pour le composant 

-La class peut definir des variable et des methodes qui peuvent est êre utilisés dans le template html.

`import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Learn angular';
 
}
`
## Affichier des variables dans le template

Pour afficher des variables dans le html on utilise les doubles accolades `{{variable}}`
Pour ce qui est des attributs html on utilise le property binding ` [attribut]="variable"`
 ` EX [src]="imageUrl ou [disable]="isDisable"`
## Transmission de variables
- Pour transmettre une variable du composant parent au composant enfant il fau créer une variable dans le compasant enfant qui contiendra la variable qui sera transmis par le parent et le décorer avec  @Input()
- Dans le code html du parent on utilise le property binding pour transmettre ensuite la variable à l'enfant

export class ParentComponent {
  title:string = 'Learn angular';
 
}

export class EnfantComponent {
  @Input() title:string;
 
}

Html du parent
<app-home [title]="title" ></app-home>

## La gestion des évènements javascript

- Pour gerer les évènements on créer une methode qui traitera l'évènement dans la class
ensuite dans le html on spécifie l'évènement et on appelle la méthde


export class AppComponent {

   getUnputValue(event:MouseEvent){
    console.log(event)
   }
}
<input type="text" (onChange)=" getInputValue($event)">

## Conditions de boucles
- Pour la condition if on utilise ` *ngIf="condition" ` dans la balise conditionné, cette balise s'affichera si la condition est remplis
- Pour les boucle on utilise ` *ngFor="type de boucle` :Ex: *ngFor=" let book of books"
Avec les boucle on peut obtenir d'autre information
  - index : position de l'élément.
  - odd :ndique si l'élément est à une position impaire
  - even :indique si l'élément est à une position paire.
  - first : indique si l'élément est à la première position.
  - last : indique si l'élément est à la dernière position.
###
<ul>
    <li *ngFor="let book of bookList; let index = index; let isFirst = first; let isOdd=odd;">
        <span>{{ index }}</span>
        <span>:</span>
        <span>{{ book.name }}</span>
        <span>( isFirst: {{ isFirst }}, isOdd: {{ isOdd }} )</span>
    </li>
</ul>

###

## Le routing
- Pour faire le routing if faut importer le module RouterModule de angular
- Ajouter à l'index.html <base href="/">  afin d'activer le routage
- Appler sa methode forRoot() `RouterModule.forRoot()`
  * forRoot prend en paramètre un tableau d'objets et chaqua objet a la structure suivante
   {
    path:"url",
    component:compasant
   }
- Utiliser la balise <router-outlet> </router-outlet> à l'endroit oùles pages doivent apparaitre
  NB: On peut utilisé Routes de angular poour typé le tableau en typescript

##
  const routes: Routes = [
  { path: '', component: HomeComponent, },
  { path: 'contact', component: ContactComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
   - # Les liens dans le html
      * `routerLink` pour indiquer le lien 
      * `routerLinkActive` pour le lien active
          
      <ul>
        <li><a routerLink="/" routerLinkActive ="active" >Home</a></li>
        <li><a routerLink="/about"  routerLinkActive ="active" >About</a></li>
        <li><a routerLink="/contact"  routerLinkActive ="active" >Contact</a></li>
      </ul>
       * Passer des paramètre aux routes

       <ul>
        <li><a [routerLink]="['url','parametre1','parametre2']">Home</a></li>
      </ul>
# Les modules

- Un module est une class typscript  decore avec @NgModule
-  @NgModule prend en paramètre un objet avec les clés suivant,tous des tableau:

  * declarations:Liste de toutes les declaration des composants  (ou directives, pipes etc...) qui seront utilisés dans ce module

  * imports : Définit la liste des dépendances du module. Il s'agit généralement de la liste des modules contenant les composants utilisés par les composants de ce module

  * exports :La liste des composants pouvant être utilisés par les modules qui importent celui-ci.