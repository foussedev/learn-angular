import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule ,Routes} from "@angular/router"

import { AppComponent } from './app.component';
import { HomeComponent } from './views/home/home.component';
import { ServicesComponent } from './views/services/services.component';
import { ContactComponent } from './views/contact/contact.component';


const routes:Routes=[
     {
      path:"",
      component:HomeComponent
     },
     {
      path:"services",
      component:ServicesComponent
     },
  {
    path: "contact",
    component: ContactComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ServicesComponent,
    ContactComponent,
  
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule.forRoot(routes),
  
  
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }