import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  
  

  constructor() { }

  ngOnInit(): void {
     
   
  }

  

  buy(event:MouseEvent){
      event?.preventDefault()

    console.log(event)
  }

  
  /**
   * Pour creer un composant en angular on proède comme suit
   */
  //Methode 1
      //Utiliser angular-cli ng g c nom_composant
   /* Methode 2
    // On import Component de @angular/core
    //On creer un classe et on l'esport 
    // On décore ma class avec @component précedement import
    //@Component prend en paramètre un objet 
    {selector:à utiliser comme balise css pour afficher le composant
    templateUrl:chemin vers le fichier html du composant
    styleUrls:[tableau de chemin des fichier css por le template html]


  
}

  */

}
